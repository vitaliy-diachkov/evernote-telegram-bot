![Telenote Logotype](src/client/public/images/logo.svg)

# Telenote

## What is Telenote?

Telenote is a [Teleram](https://telegram.org/ "Telegram official website") Bot that makes you able to save any forwarded message as a note in your [Evernote](https://evernote.com/ "Evernote official website") profile.

## Getting started

To get start with Telenote go to our [website](http://telenote.tk/) link or directly follow the Telenote Bot [link](https://t.me/evernote_noting_bot) and do the following:

1. **Execute the** `/login` **command**, by doing this you would get the authorization URL. Follow the given link to authorize our application in your Evernote profile so that the bot will be able to create a notes on your behalf.
2. **Copy the obtained** `/token` **command and paste it to bot**. This will save your Evernote access token into our Telegram Bot storage, so bot whould be able to use it any time the token is needed.
3. **Forward a message to Telegram Bot from a third-party chat.** A bot will automatically save it in your Evernote profile and send you a message on success. If you would have any problems while using Telenote Bot, please, [contact us](mailto:diachkovspecial@gmail.com) and provide the details.

## Development & Deployment

Any project information, including the deployment guide and project documentation can be found in [project specification](src/README.md) readme file.
