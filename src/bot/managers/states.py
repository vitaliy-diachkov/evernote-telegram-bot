from aiogram.dispatcher import FSMContext


class TokenStateManager:
    """
    A class that manages the token bot state.
    """

    @staticmethod
    async def get(state: FSMContext):
        """
        Returns the token stored in storage.
        """

        async with state.proxy() as data:
            return data.get('token', None)

    @staticmethod
    async def set(state: FSMContext, token: str):
        """
        Sets the token value stored in storage.
        """

        async with state.proxy() as data:
            data['token'] = token
            return token

    @staticmethod
    async def clear(state: FSMContext):
        """
        Deleted the token value from storage.
        """

        async with state.proxy() as data:
            data['token'] = None
