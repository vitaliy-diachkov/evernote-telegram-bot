"""
A custom Exception subclasses that can be raised and proccessed during
the application work.
"""


class ParseTokenError(Exception):
    """
    This exception raises while token parsing if the given data had
    invalid format.
    """
    pass


class FetchError(Exception):
    """
    This error raises if sending request (e.g. using the custom `fetct`
    function) ended in failure.
    """
    pass


class CommandError(Exception):
    """
    This error raises if command execution ended in failure.
    """
    pass
