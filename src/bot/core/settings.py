"""
Bot configuration file
"""

import os


# Telegram settings
TELEGRAM_API_TOKEN = os.environ.get('TELEGRAM_API_TOKEN')

# Storage settings
REDIS_HOST = os.environ.get('REDIS_HOST', '127.0.0.1')

# API settings
API_URL = os.environ.get('API_URL')
