"""
An utility functions that parses incoming data
"""

from core.exceptions import ParseTokenError


def parse_user_token(message_text: str):
    """
    Prases user token from given string. Usually, the given string is
    formatted like: /token <tolen_value>. So, basically, it just retuls
    the <token_value> from string.
    """

    if not '/token' in message_text:
        raise ParseTokenError('Ivalid input data')

    splited_text = message_text.split()

    if len(splited_text) != 2:
        raise ParseTokenError('Invalid format! Should be `/token <token>`')

    return splited_text[1]
