from typing import Dict

from aiogram.types import Message

from core.exceptions import FetchError

from managers.api import create_note


async def perform_note_creation(body: str, token: str) -> Dict[str, str]:
    """
    A create_note API function wrapper for handling the note creation
    by user.

    Args:
        body (str): A note body to be created
        token (str): An Evernote OAuth user token that belongs to user
            who is created note owner.

    Returns:
        Dict[str, str]: {'message': <message text for user>}
    """
    if not body:
        return {
            'message': 'It is not allowed to create notes with empty body'
        }

    try:
        response = await create_note(body, token)
    except FetchError as e:
        return {'message': str(e)}

    title = response.get('title', None)

    if not title:
        return {'message': (
            'Unfortunately, something went wrong. Please, try again later or '
            'contact administrator if the problem would not be resolved.'
        )}

    return {
        'message': f'A note with title **"{title}"** was successfully created'
    }
