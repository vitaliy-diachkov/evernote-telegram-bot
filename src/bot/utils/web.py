"""
A module that contains utility functions to interact with web.
"""

import aiohttp
from aiohttp.client_exceptions import ClientConnectionError

from core.exceptions import FetchError


# Allowed HTTP methods to use
_http_methods = ('get', 'post', 'put', 'patch', 'delete')


async def fetch(url: str, method:str = 'get', **kwargs) -> dict:
    """
    Sends HTTP request and retrieves data from response.

    Args:
        url (str): A URL that request should be sent to.
        method (str): A HTTP method that should be used.
        **kwargs (dict): A list of request props i.e. headers, body
            and so on.

    Raises:
        FetchError: On invalid HTTP method call

    Returns:
        diict: Deserialized JSON data that was retrieved from response.
    """

    async with aiohttp.ClientSession() as session:
        if method not in _http_methods:
            raise FetchError(
                f'Unknown method {method}, must be in {_http_methods}'
            )

        send_request = getattr(session, method)

        try:
            response = await send_request(url, **kwargs)
        except ClientConnectionError as e:
            raise FetchError('Unenable to connect to server')

        if response.status >= 300:
            raise FetchError(
                f'A server responded with status {response.status}'
            )

        return await response.json()
