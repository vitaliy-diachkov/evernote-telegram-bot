# Bot specification

A Telenote's telegram bot was written using the following third-party libraries, packages, frameworks:

- [aiogram](https://github.com/aiogram/aiogram "aiogram Github repository");
- [aiohttp](https://github.com/aio-libs/aiohttp "aiohttp Github repository");
- [poetry](https://github.com/python-poetry/poetry "poetry Github repository");
- and their dependencies ...

## Deploying with Poetry

To daploy a bot using poetry do the following:

1. Install all the bot dependencies: `poetry install`
2. Set the `TELEGRAM_API_TOKEN`, `REDIS_HOST` and `API_URL` variables.
3. Run Redis and Telenote API services.
4. (optional) Run the client application in order to have an Evernote OAuth URL callback rendered. You can always copy manually the token from callback URL query parameters and pass it to bot.
5. Exceute `poetry run python3 __main__.py`.
