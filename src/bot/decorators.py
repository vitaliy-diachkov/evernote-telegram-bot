from aiogram.types import Message
from aiogram.dispatcher import FSMContext

from managers.states import TokenStateManager


def token_required(func):
    """
    A decorator that verifies that user token (Evernote OAuth access token)
    was set and passes it to message handler function on success.
    """
    async def wrapper(message: Message, state: FSMContext):
        token = await TokenStateManager.get(state)

        if not token:
            await message.reply(
                'Unfortunately, you are unenable to craete new notes via '
                'NoteBot untill you set your token. Please, use /login and '
                '/token commands in order to make the things working.'
            )
        else:
            await func(message, token)
    return wrapper
