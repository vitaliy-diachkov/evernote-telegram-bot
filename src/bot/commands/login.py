from typing import Dict, Any

from managers.api import get_authorize_url

from .base import BaseAsyncCommand


class LoginCommand(BaseAsyncCommand):
    """
    Handles user /login command. Command signature:
        /login -- sends the auhorization URL to user. By following this
            URL user would get on the Evernote OAuth confirm page. If
            user will perform authorization - application would become
            able to create notes for him by using the access token he got.
        /login help -- shows the help message

    Attributes:
        command_regex (str): A regular expression to check if the command
            text is valid and matches the command signature.

    Args:
        payload (str): The command message text
        chat_id (int): Current user with bot chat ID
    """
    command_regex: str = '/login($|\shelp)'

    def __init__(self, payload, chat_id: int):
        super().__init__(payload)
        self.chat_id = chat_id

    async def execute(self) -> Dict[str, str]:
        """
        Runs the command. Performs actions depends on command text.

        Raises:
            CommandError: If the command execution has failed. The
                CommandError message can be passed to user.

        Returns:
            Dict[str, str]: {'message': 'text'}
        """
        command_text = await self.match_payload()

        if command_text == '/login':
            return await self._exceute_login_perform()
        if command_text == '/login help':
            return self._execute_login_help()

    def _execute_login_help(self):
        """
        Returns /login command help text.
        """
        return {'message': (
            'A `/login` command signature:\r\n\r\n'
            '  `/login` -- get the login (authorization) URL;\r\n'
            '  `/login help` -- get the message text;\r\n'
        )}

    async def _exceute_login_perform(self):
        """
        Gets the authorization URL using the API and returns it. Raises
        CommandError on fetch failure.
        """
        try:
            authorize_url = await get_authorize_url(self.chat_id)
        except FetchError as e:
            raise CommandError(
                'Unenable to fetch authorization URL. Please, try again '
                'later or contact administrator if the problem would not '
                'resolve.'
            )

        return {'message': f'Follow the link below\r\n\r\n{authorize_url}'}
