import re

from typing import Dict, Any

from aiogram.dispatcher import FSMContext

from core.exceptions import CommandError, FetchError

from managers.states import TokenStateManager
from managers.api import validate_user_token

from .base import BaseAsyncCommand


class TokenCommand(BaseAsyncCommand):
    """
    /token command executor. Command signature:
        /token -- gets the current token
        /token clear -- deletes the current token
        /token <value> -- resets token value
        /token help -- shows help message

    Attributes:
        command_regex (str): A regular expression that checks if message
            text matchges the command signature

    Args:
        payload (str): A message text has been passed through
        state (FSMContext): A state of token has been stored in
            FSMStorage (redis for now)
    """
    command_regex: str = '/token($|\s\S*)'

    def __init__(self, payload: str, state: FSMContext):
        super().__init__(payload)
        self.state = state

    async def execute(self) -> Dict[str, Any]:
        """
        Runs the /token command depends on it's parameters

        Raises:
            CommandError: If the command execution ended in failure

        Returns:
            Dict[str, str]: {'message': <message for user>}
        """
        command_text = await self.match_payload()

        if command_text == '/token':
            return await self._execute_token_get()
        if command_text == '/token clear':
            return await self._execute_token_clear()
        if command_text == '/token help':
            return self._execute_token_help()
        return await self._execute_token_set(command_text)

    def _execute_token_help(self):
        """
        Returns the help message
        """
        return {'message': (
            'A `/token` command signature:\r\n\r\n'
            '  `/token` -- get curent token;\r\n'
            '  `/token <value>` -- set a new token;\r\n'
            '  `/token clear` -- remove current token;\r\n'
            '  `/token help` -- get helper text.\r\n\r\n'
            'What is token? Token is a unique key you can get to manage your '
            'Evernote profile data (i.e. notes, notebooks etc). To get token '
            'use /login command. To learn more run `/login help`.'
        )}

    async def _execute_token_get(self):
        """
        Returns current token
        """
        token = await TokenStateManager.get(self.state)
        return {'message': f'Current token is `{token}`'}

    async def _execute_token_clear(self):
        """
        Clears current token and returns message on success
        """
        await TokenStateManager.clear(self.state)
        return {'message': 'A token value was successfully cleared'}

    async def _execute_token_set(self, command_text: str):
        """
        Validates the given token value and resets the current token value
        if the given token is valid.
        """
        token = await self._parse_token(command_text)

        try:
            is_valid = await validate_user_token(token)
        except FetchError as e:
            raise CommandError(
                'Token validation failed on server-side. Please, try again '
                'or contact administrator if the problem would not be '
                'resolved.'
            )

        if not is_valid:
            raise CommandError('Token validation failed: invalid token!')

        await TokenStateManager.set(self.state, token)
        return {'message': 'A new token value has been set'}

    async def _parse_token(self, command_text: str) -> str:
        """
        Gets the token value from a command text string
        """
        command_words = command_text.split()

        if len(command_words) != 2:
            raise CommandError('Missing token value to set.')

        token = command_words[1]
        return token
