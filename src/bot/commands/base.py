import re

from typing import Dict

from core.exceptions import CommandError


class BaseAsyncCommand:
    """
    BaseAsyncCommand is an abstract base class for handling user command
    execution, passed from Telegram Bot.

    To implement the main features override the .command_regex property
    and .execute() method.

    Attributes:
        command_regex (str): A regular expression that checks if the given
            payload matches the command signature. Define this property
            when creating a subclass.

    Args:
        payload (str): A command message got from user
    """
    command_regex: str = None

    def __init__(self, payload: str):
        self._payload = payload

    def get_payload(self) -> str:
        """
        Returns the given payload. Override this method in order to add
        extra logic to payload parsing (?)
        """
        return self._payload

    def get_command_regex(self):
        """
        Returns the compiled command regular expression. Override this
        method to add extra logic for formatting command regex
        """
        if not self.command_regex:
            raise NotImplementedError(
                'A .command_regex parameter should be set'
            )

        return re.compile(self.command_regex)

    async def match_payload(self) -> str:
        """
        Checks if the given payload matches command regular expression.

        Raises:
            CommandError: If the payload did not matched

        Returns:
            str: A valid payload
        """
        payload = self.get_payload()
        command_regex = self.get_command_regex()

        if not command_regex.match(payload):
            raise CommandError(
                'A given string does not match command signature'
            )

        return payload

    async def execute(self) -> Dict[str, str]:
        """
        Override this method to handle command execution. Always get the
        command text from .match_payload() method or create your own method
        that would be composed with .match_payload()
        """
        command_text = self.match_payload()
        raise NotImplementedError('An .exceute() method should be defined')
