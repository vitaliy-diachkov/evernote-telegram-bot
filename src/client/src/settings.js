// Telegram Bot URL
export const botURL = 'https://t.me/evernote_noting_bot';

// Backend URL to send requests
export const apiURL = '/api';
