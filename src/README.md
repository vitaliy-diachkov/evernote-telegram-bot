# Project specification

## General

Telenote simple links the public [Telegram](https://core.telegram.org/bots/api "Telegram Bot API")and [Evernote](https://dev.evernote.com/ "Evernote API homepage") API to make it able to
create Evernote notes based on Telegram messages.

See the next file for implementation details:

- [bot specification](bot/README.md);
- [API specification](api/README.md);
- [client-side application specification](client/README.md);

## Deployment

To deploy this project do the following (execute command while being in project root directory):

1. Configure the environemnt: check the [example.env](../.deploy/example.env) for a list of environment variables needed.
1. Build the docker images `docker-compose -f .deploy/docker-compose build`.
2. Run the built containers `docker-compose -f .deploy/docker-compose up`.
