"""
Database client definition. Import the `db` variable from this module
in order to access the database features.
"""

from motor.motor_asyncio import AsyncIOMotorClient

from core import settings


client = AsyncIOMotorClient(
        f'mongodb://{settings.MONGO_USERNAME}:{settings.MONGO_PASSWORD}'
        f'@{settings.MONGO_HOST}'
)

db = client['TelegramEvernote']
