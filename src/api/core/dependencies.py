"""
FastAPI `dependencies` module. The dependencies are defined as:

"Dependency Injection" means, in programming, that there is a way
for your code (in this case, your path operation functions) to declare
things that it requires to work and use: "dependencies".

And then, that system (in this case FastAPI) will take care of doing
whatever is needed to provide your code with those needed dependencies
("inject" the dependencies).

This is very useful when you need to:
    - Have shared logic (the same code logic again and again).
    - Share database connections.
    - Enforce security, authentication, role requirements, etc.
    - And many other things...
"""

from fastapi import Header, Depends, HTTPException

from apps.tokens.utils import check_user_token


async def contains_authorization_header(authorization: str = Header(None)):
    """
    A dependency that checks whether Authorization: Token HTTP-header was
    provided within the request.

    Args:
        authorization (str): Authorization HTTP-header value (i.e.
            `Token ASDFGH12345`)

    Returns:
        str: Parsed token from HTTP header (i.e. `ASDFGH12345`)
    """

    if not authorization:
        # if not authorization header was provided
        raise HTTPException(
            status_code=401,
            detail='Authorization header was not provided'
        )

    splited_header = authorization.split()

    if len(splited_header) != 2:
        # if authorization header contains more then 2 words
        raise HTTPException(
            status_code=400,
            detail=(
                'Should include header Authorization: Token `value`, not %s'
                % authorization
            )
        )

    # Authorization type, token value
    auth_type, token = splited_header

    if auth_type != 'Token':
        # Check is Authorization type was set as Token
        raise HTTPException(
            status_code=400,
            detail=(
                'Invalid Authorization header type (should be Token, not %s)'
                % auth_type
            )
        )

    return token


async def contains_valid_token(
    token: str = Depends(contains_authorization_header)
):
    """
    Checks whether the provided in Authorization HTTP-header token
    is valid (presented in users database collection).

    Args:
        token (str): A token to be validated

    Returns:
        str: A validated token
    """

    is_valid = await check_user_token(token)

    if not is_valid:
        raise HTTPException(
            status_code=401,
            detail=f'A token {token} does not exist'
        )

    return token
