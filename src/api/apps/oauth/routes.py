"""
An API routes that refers to Evernote OAuth
"""

from fastapi import APIRouter, Request

from core import settings

from apps.oauth.models import UserChat
from apps.oauth.shortcuts import retrieve_authorize_url


router = APIRouter()


@router.get('/authorize-url/', name='authroize_url')
async def get_authorize_url(user_chat: UserChat, request: Request):
    """
    Returns the URL to authorize the application with user's personal
    evernote account.

    Args:
        user_char (UserChat): A user with bot chat representation
        request (Request): A client's request

    Returns:
        Dict[str, str]: A result dictonary with authorization URL
    """

    url = await retrieve_authorize_url(
        settings.OAUTH_CALLBACK_URL,
        user_chat.chat_id
    )

    return {'authorize_url': url}
