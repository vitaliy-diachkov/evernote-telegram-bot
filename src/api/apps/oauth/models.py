"""
A pydantic BaseModel subclasses that refers to Telegram Bot API.
"""

from pydantic import BaseModel


class UserChat(BaseModel):
    """
    A bot with user telegram chat representation.

    Attributes:
        chat_id (int): A unique telegram chat identifier
    """

    chat_id: int
