"""A module that contains different apps.

The `app` is a FastAPI router with route handlers and some helper methods
and classes (e.g. models, factories, utility functions and shurtcuts)
encapsulated in it.
"""
