"""
A module that contains classes to manage the users temporary and access
tokens provided by Evernote OAuth API.
"""

from typing import Dict

from core import settings
from core.database import db

from apps.notes.utils import get_evernote_client

from apps.tokens.exceptions import RetrieveAccessTokenError


class TemporaryTokenDataManager:
    """
    A class to manage the users temporary token data.
    """

    @staticmethod
    async def get(callback_url: str):
        """
        Gets the temporary token and authorize URL. After following the
        authorize URL user can grant permission to this application to
        access this notes data.

        Args:
            callback_url (str): A URL that user will be redirected
                after authorizing this application in this Evernote
                account.

        Returns:
            Dict[str, str]: A dictonary contains temporary token,
                temporary token secret string and authorization URL.
        """

        client = get_evernote_client()
        request_token = client.get_request_token(callback_url)
        authorize_url = client.get_authorize_url(request_token)
        return {
            'token': request_token['oauth_token'],
            'secret': request_token['oauth_token_secret'],
            'url': authorize_url
        }

    @staticmethod
    async def save(chat_id: int, token_data: Dict[str, str]):
        """
        Saves the temporary token data in database.

        Args:
            chat_id (int): A user with bot telegram chat unqiue ID.
            token_data (Dict['str, str']): A dictonary that contains
                data about users temporary token and temporary token
                secret,

        Returns:
            UpdateResult: A mongo engine class contains information
                about the proccessed updates in database.
        """

        return await db.users.update_one(
            {
                'chat_id': chat_id
            },
            {
                '$set': {
                    'tokens.temporary': {
                        'token': token_data['token'],
                        'secret': token_data['secret']
                    }
                }
            },
            upsert=True
        )


class AccessTokenDataManager:
    """
    A class to manage the users access token data.
    """

    @classmethod
    async def _get_token_secret(cls, temporary_token: str):
        """
        Private method to get the users temporary token secret
        from project database.

        Raises:
            RetrieveAccessTokenError: If the token secret value was not
                found by given temporary token value.

        Returns:
            str: Token secret value from database
        """

        user_data = await db.users.find_one({
            'tokens.temporary.token': temporary_token
        })

        if user_data:
            return user_data['tokens']['temporary']['secret']

        raise RetrieveAccessTokenError('Invalid temporary token')

    @classmethod
    async def get(cls, temporary_token: str, verifier: str):
        """
        Gets the users access token.

        Args:
            temporary_token (str): User's OAuth temporary token
            verifier (str): Generated verifier token that shows whether
                user has granted permission to this application to
                access his notes data.

        Raises:
            RetrieveAccessTokenError: In case the access token was not
                fetched.

        Returns:
            Dict[str, str]: A result dictonary, that contains
                retrieved user's access token and some additional data.
                More on https://github.com/evernote/evernote-sdk-python3/blob/master/lib/evernote/api/client.py#L79
        """

        client = get_evernote_client()
        secret = await cls._get_token_secret(temporary_token)

        if not verifier:
            raise RetrieveAccessTokenError('Empty verifier')

        try:
            return client.get_access_token_dict(
                oauth_token=temporary_token,
                oauth_token_secret=secret,
                oauth_verifier=verifier
            )
        except KeyError:
            raise RetrieveAccessTokenError('Invalid temporary token/verifier')

    @classmethod
    async def get_or_create(cls, temporary_token: str, verifier: str):
        """
        Searches for the given temporary token in database and if it
        was found - returns the existing access token. If not - fetches
        new token from Evernote API and saves it to database.

        Args:
            temporary_token (str): An Evernote OAuth temporary token
            verifier (str): A user verifier string (see .get() method
                documentation for more details)

        Raises:
            RetrieveAccessTokenError: If the token was not fetched from
                Evernote API. Provides a detail description of the
                possible source of the problem.

        Returns:
            str: User access token
        """

        stored_in_db = await db.users.find_one({
            'tokens.temporary.token': temporary_token
        })

        if (
            stored_in_db
            and stored_in_db.get('tokens')
            and stored_in_db['tokens'].get('access')
        ):
            return stored_in_db['tokens']['access']

        token_data = await cls.get(temporary_token, verifier)
        await cls.save(temporary_token, token_data)
        return token_data['oauth_token']

    @staticmethod
    async def save(temporary_token:str, data: Dict[str, str]):
        """
        Saves user access token into project database.

        Args:
            temporary_token (str): A users temporary token as a user
                unique identifier in collection.
            data (Dict[str, str]): A data retrieved from Evernote API,
                contains access token and some extra values. More on
                https://github.com/evernote/evernote-sdk-python3/blob/master/lib/evernote/api/client.py#L79
        """

        return await db.users.update_one(
            {
                'tokens.temporary.token': temporary_token
            },
            {
                '$set': {
                    'tokens.access': data['oauth_token'],
                    'edam': {
                        'shard': data['edam_shard'],
                        'note_store_url': data['edam_noteStoreUrl'],
                        'user_id': data['edam_userId'],
                        'web_api_url_prefix': data['edam_webApiUrlPrefix'],
                        'expires': data['edam_expires']
                    }
                }
            }
        )
