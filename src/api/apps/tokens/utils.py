from core.database import db


async def check_user_token(token: str) -> bool:
    """
    Checks if the given token exists in database.

    Args:
        token (str): A users access token to be checked.

    Returns:
        bool: A logic statement that shows whether the given token
            was found in database or not.
    """

    return bool(await db.users.find_one({'tokens.access': token}))
