"""
An API entrypoint, containing the FastAPI app definition and routing
configuration
"""
from fastapi import Depends, FastAPI
from fastapi.security import OAuth2PasswordBearer
from fastapi.middleware.cors import CORSMiddleware

from apps import oauth, tokens, notes


app = FastAPI()

app.include_router(oauth.router, prefix='/api/oauth', tags=['oauth'])
app.include_router(tokens.router, prefix='/api/tokens', tags=['tokens'])
app.include_router(notes.router, prefix='/api/notes', tags=['notes'])
